// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function f = flps_numbereval ( flpn )
    //   Returns the value of the current floating point number.
    //
    // Calling Sequence
    //   f = flps_numbereval ( flpn )
    //
    // Parameters
    //   flpn: a floating point number, or a list of floating point numbers
    //   f: a 1x1 matrix of doubles, the value
    //
    // Description
    //   Returns an evaluation of the floating point number flpn.
    //   If flpn is a list,  returns a column matrix containing the
    //   values of the floating point numbers in this list.
    //
    // Examples
    //   // Evaluate one floating point number
    //   x = 4;
    //   radix = 2;
    //   p = 3;
    //   e = 3;
    //   flps = flps_systemnew ( "format" , radix , p , e )
    //   flpn = flps_numbernew ( "double" , flps , x )
    //   // This should be f = 4
    //   f = flps_numbereval ( flpn )
    //
    //   // Evaluate all the floating point numbers in a system
    //   flps = flps_systemnew ( "format" , 2 , 3 , 3 );
    //   listflpn = flps_systemall ( flps );
    //   // This should be -14, -12, -10, -8, ..., 12, 14
    //   f = flps_numbereval ( listflpn )
    //
    // Authors
    // Michael Baudin, 2008 - 2010
    //
    // Bibliography
    //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

    [lhs, rhs] = argn()
    apifun_checkrhs ( "flps_numbereval" , rhs , 1 )
    apifun_checklhs ( "flps_numbereval" , lhs , 1 )
    //
    apifun_checktype ( "flps_numbereval" , flpn , "flpn" , 1 , ["TFLNUMB" "list"] )
    //
    if ( typeof ( flpn ) == "TFLNUMB" ) then
        flps = flpn.flps
        if ( flps_numberisnan(flpn) ) then
            f = %nan
        elseif ( flps_numberisinf(flpn) ) then
            if ( flpn.s==1 ) then
                f = -%inf
            else
                f = %inf
            end
        else

            f = flpn.M * flps.radix^(flpn.e - flps.p + 1)
        end
    elseif ( typeof ( flpn ) == "list" ) then
        n = size(flpn);
        f = zeros(n,1)
        for i = 1 : n
            if ( typeof ( flpn(i) ) <> "TFLNUMB" ) then
                errmsg = msprintf(gettext("%s: Element #%d in the list is not a floating point number."),"flps_numbereval",i)
                error ( errmsg )
            end
            f(i) = flps_numbereval ( flpn(i) )
        end
    else
        errmsg = msprintf(gettext("%s: Unknown type %s for input argument flpn.") , "flps_numbereval" , typeof ( flpn ) )
        error ( errmsg )
    end
endfunction

