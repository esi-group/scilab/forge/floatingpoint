// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ f , e ] = flps_frexp ( varargin )
  //   Returns the exponent and fraction.
  //
  // Calling Sequence
  // f = flps_frexp ( x )
  // f = flps_frexp ( x , b )
  // f = flps_frexp ( x , b , algo )
  // [ f , e ] = flps_frexp ( ... )
  //
  // Parameters
  //   x : a matrix of doubles, the floating point number to decompose
  //   b : a matrix of floating point integers, must be positive, the radix (default = 2)
  //   algo : a 1x1 matrix of strings, "v1" or "v2" (default is "v1"). If algo="v1", then, on output, f satisfies 1/b <= abs(f) < 1. If algo="v2", then, on output, f satisfies 1 <= abs(f) < b.
  //   f : a matrix of doubles, the fraction
  //   e : a matrix of floating point integers, the exponent
  //
  // Description
  //   The exponent and the fraction are so that x = f * b^e.
  //
  //   This function should not be used in practice: use Scilab's function frexp, which
  //   performs the same, but faster and with more accuracy.
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  // If x is a scalar and b is a matrix, then x is expanded to the size of b.
  // If b is a scalar and x is a matrix, then b is expanded to the size of x.
  //
  // The following cases are so that the inequality on f does not hold.
  //   <itemizedlist>
  //   <listitem><para>If x is zero, then f is zero and e is zero.</para></listitem>
  //   <listitem><para>If x is nan, then f is nan and e is zero.</para></listitem>
  //   <listitem><para>If x is infinity, then f is infinity and e is zero.</para></listitem>
  //   </itemizedlist>
  //
  // Examples
  //   [ f , e ] = flps_frexp ( 1 , 2 ) // f=0.5, e=1
  //   [ f , e ] = flps_frexp ( 1 , 3 ) // f=1/3, e=1
  //   [ f , e ] = flps_frexp ( 1 , 3 , "v1" ) // f=1/3, e=1
  //   [ f , e ] = flps_frexp ( 1 , 3 , "v2" ) // f=1, e=0
  //
  //   // With matrix input
  //   b = [3 3 3 3];
  //   x = [1 2 3 4];
  //   [ f , e ] = flps_frexp ( x , b )
  //
  //   // With x scalar and b matrix
  //   b = [2 3;5 7];
  //   x = 2;
  //   [ f , e ] = flps_frexp ( x , b )
  //
  //   // With b scalar and x matrix
  //   x = [2 3;5 7];
  //   b = 2;
  //   [ f , e ] = flps_frexp ( x , b )
  //
  //   // Check IEEE values
  //   x = [+0 -0 %inf -%inf %nan -%nan];
  //   expected_f = [+0 -0 %inf -%inf %nan -%nan];
  //   expected_e = [0   0 0    0     0    0];
  //   [ f , e ] = flps_frexp ( x );
  //
  //   // Check extreme values
  //   largestnormal = 9007199254740991 * 2^(1023-53+1);
  //   [ f , e ] = flps_frexp ( largestnormal ) // f=1-%eps/2, e=1024
  //   // The expression 2^e overflows
  //   f*2^e
  //   // This expression recovers x
  //   (f*2) * (2^(e-1))
  //
  // Authors
  // Michael Baudin, 2008 - 2010
  //
  // Bibliography
  //   "Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_frexp" , rhs , 1:3 )
  apifun_checklhs ( "flps_frexp" , lhs , 1:2 )
  //
  x = varargin(1)
  b = argindefault ( rhs , varargin , 2 , 2 )
  algo = argindefault ( rhs , varargin , 3 , "v1" )
  //
  apifun_checktype ( "flps_frexp" , x , "x" , 1 , "constant" )
  apifun_checktype ( "flps_frexp" , b , "b" , 2 , "constant" )
  apifun_checktype ( "flps_frexp" , algo , "algo" , 3 , "string" )
  //
  apifun_checkscalar ( "flps_frexp" , algo , "algo" , 3 )
  //
  apifun_checkgreq ( "flps_frexp" , b , "b" , 2 , 2 )
  for k = 1 : size(b,"*")
    if ( find(b(k)==primes(100))==[] ) then
      warning(msprintf(gettext("%s: Radix %d at entry #%d is not prime."),"flps_frexp",b(k),k))
    end
  end
  apifun_checkoption ( "flps_frexp" , algo , "algo" , 3 , ["v1" "v2"] )
  //
  if ( size(b,"*") == 1 & size(x,"*") <> 1 ) then
    b = b*ones(x)
  end
  if ( size(x,"*") == 1 & size(b,"*") <> 1 ) then
    x = x*ones(b)
  end
  //
  f = zeros(x)
  e = zeros(x)
  //
  // Parameters for doubles
  emax = 1023
  //
  // Compute the log-b of |x|
  k = find(x<>0)
  if ( k <> [] ) then
    l(k) = log(abs(x(k)))./log(b(k))
    //
    select algo
    case "v1" then
      e(k) = min(floor(l(k)),emax) + 1
    case "v2" then
      e(k) = min(floor(l(k)),emax)
    end
    //
    // Avoid unnecessary overflow, underflow
    kp = find(e(k)>0)
    f(k(kp)) = x(k(kp)) .* b(k(kp)).^-e(k(kp))
    kn = find(e(k)<=0)
    f(k(kn)) = x(k(kn)) ./ b(k(kn)).^e(k(kn))
    //
    // Fix if necessary
    select algo
    case "v1" then
      kf = find(abs(f(k))<1 ./ b(k))
    case "v2" then
      kf = find(abs(f(k))<1)
    end
    if ( kf <> [] ) then
      f(k(kf)) = f(k(kf)) .* b(k(kf))
      e(k(kf)) = e(k(kf)) - 1
    end
  end
  //
  // Process Infinities
  k = find(abs(x)==%inf)
  e(k) = 0
  f(k) = x(k)
  //
  // Process Nans
  k = find(isnan(x))
  e(k) = 0
  f(k) = x(k)
endfunction
//
// Analysis of a special case.
// Consider algo="v1".
// Consider x = 4503599627370495 * 2^(-1022-53+1).
// This is the largest denormal.
// We have, exactly, log2(x) = -1022.00000000000000032034...
// Then, exactly, floor(log2(x)) + 1 = -1023 + 1 = -1022.
// But lk = fl(log2(x))=-1022. This changes everything, but is true.
// That is, -1022 is the floating point number which is the closest to -1022.00000000000000032034...
// Then ek = floor(lk) = -1022 + 1 = -1021, i.e. ek is to large.
// Hence, a small change in lk, makes a big change in floor(lk).
// Hence, floor is ill-conditionned for this particular x.
// To fix this, we check the inequality abs(f(k))>=1 ./ b(k).
// If not, we update f and e accordingly.
//
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

