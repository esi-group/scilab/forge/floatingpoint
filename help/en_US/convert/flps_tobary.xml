<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from flps_tobary.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="flps_tobary" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>flps_tobary</refname><refpurpose>Returns the digits of the b-ary decomposition.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   d = flps_tobary ( x )
   d = flps_tobary ( x , b )
   d = flps_tobary ( x , b , p )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> 1x1 matrix of doubles, the floating point number to decompose</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the radix (default b=2)</para></listitem></varlistentry>
   <varlistentry><term>p :</term>
      <listitem><para> a 1x1 matrix of floating point integers, the precision (default p=53)</para></listitem></varlistentry>
   <varlistentry><term>d :</term>
      <listitem><para> a p-by-1 matrix, the digits of the b-ary decomposition of x.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Returns the digits of the b-ary decomposition of x in base b with
precision p.
Given a number x and a radix b such that
0 &lt;= x &lt; b, and given a precision p,
returns the digits d(i), for i = 1, 2, ..., p,
with x = d(1) + d(2)/b + d(3)/b^2 + ... + d(p-1)/b^(b-1).
   </para>
   <para>
Any optional input argument equal to the empty matrix is replaced by its default value.
   </para>
   <para>
Caution : there is no specific rounding mode guaranteed
by this function, which implies that the last digit
is most of the times unsafe.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// This is d = [1 0]
d = flps_tobary ( 1 , 2 , 2 )
// This is d = [0 1 0 1]
x = 1/3+1/3^3
d = flps_tobary ( x , 3 , 4 )
// This is d = [1 1 0 1 0 1]
x = 1 + 1/2 + 1/2^3 + 1/2^5
d = flps_tobary ( x , 2 , 6 )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, 2010</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010</para>
</refsection>
</refentry>
