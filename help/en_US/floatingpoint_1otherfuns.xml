<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
-->
<refentry
  xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:db="http://docbook.org/ns/docbook"
  version="5.0-subset Scilab"
  xml:lang="fr"
  xml:id="floatingpoint_other">

  <refnamediv>
    <refname>Other functions</refname>

    <refpurpose>Other functions</refpurpose>
  </refnamediv>

  <refsection>
    <title>Purpose</title>

    <para>
    In this page, we gather built-in Scilab functions for floating point numbers.
    These functions are not provided by the current external module but are
    briefly summarized here for reference.
    </para>

    <para>
    The following functions allows to manage the IEEE mode and to
    inquire about current settings:
    <itemizedlist>
    <listitem>nearfloat : get previous or next floating-point number</listitem>
    <listitem>number_properties : determine floating-point parameters</listitem>
    <listitem>frexp : dissect floating-point numbers into base 2 exponent and mantissa</listitem>
    <listitem>ieee : set floating point exception mode</listitem>
    </itemizedlist>
    The following elementary functions allows to compute various
    powers and logs involved in floating point conversions:
    <itemizedlist>
    <listitem>nextpow2 : next higher power of 2</listitem>
    <listitem>log2 : base 2 logarithm</listitem>
    <listitem>log10 : base 10 logarithm</listitem>
    </itemizedlist>
    The following functions allows to manage inf in complex numbers:
    <itemizedlist>
    <listitem>imult : multiplication by i the imaginary unitary</listitem>
    <listitem>complex : create a complex number from its real and imaginary parts</listitem>
    </itemizedlist>
    The following functions allows to convert to and from various
    representations of numbers:
    <itemizedlist>
    <listitem>hex2dec : conversion from hexadecimal representation to integers</listitem>
    <listitem>bin2dec : integer corresponding to a binary form</listitem>
    <listitem>base2dec : conversion from base b representation to integers</listitem>
    <listitem>dec2bin : binary representation</listitem>
    <listitem>dec2hex : hexadecimal representation of integers</listitem>
    </itemizedlist>
    </para>

    <para>
    Other arbitrary base conversion functions are provided in the "number" module.
    </para>

   <programlisting role="example"><![CDATA[
atomsInstall("number")
   ]]></programlisting>

  </refsection>


  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>2010 - DIGITEO - Michael Baudin</member>
    </simplelist>
  </refsection>

</refentry>
