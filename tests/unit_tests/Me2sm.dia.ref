// Copyright (C) 2008 - 2010 - Michael Baudin
// <-- CLI SHELL MODE -->
//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then bugmes();quit;end
endfunction
radix = 2;
p = 53;
//
M= 9007199254740991;
e= 0;
expected_s = 0;
expected_m = 1.9999999999999997779554;
[s,m] = flps_Me2sm ( radix , p , M , e );
assert_equal ( s , expected_s );
assert_equal ( m , expected_m );
//
// Test +0
[s,m] = flps_Me2sm ( radix , p , +0 , 0 );
assert_equal ( s , 0 );
assert_equal ( m , 0 );
//
// Test -0
[s,m] = flps_Me2sm ( radix , p , -0 , 0 );
assert_equal ( s , 1 );
assert_equal ( m , 0 );
//
// Test +1
[s,m] = flps_Me2sm ( radix , p , +1 , 0 );
assert_equal ( s , 0 );
assert_equal ( m , 2^-52 );
//
// Test -1
[s,m] = flps_Me2sm ( radix , p , -1 , 0 );
assert_equal ( s , 1 );
assert_equal ( m , 2^-52 );
