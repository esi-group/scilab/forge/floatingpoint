// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
data = flps_datasetread ( dataset , "#" , "," , %f );
csize = size(data);
assert_equal ( csize , [37 6] );
// Get the first column
x = evstr(data(:,1));
assert_equal ( x(1:4) , [2.0479999999999997D+3 ; -2.0479999999999997D+3 ; 1024 ; -1024 ] );
//
// Use default parameters
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
data = flps_datasetread ( dataset );
csize = size(data);
assert_equal ( csize , [37 6] );
// Get the first column
x = evstr(data(:,1));
assert_equal ( x(1:4) , [2.0479999999999997D+3 ; -2.0479999999999997D+3 ; 1024 ; -1024 ] );
//
// COMMENTED BECAUSE VERBOSE DISPS ABSOLUTE PATH (CANNOT BE TESTED)
// Set verbose to %t
//path=flps_getpath();
//dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
//data = flps_datasetread ( dataset , [] , [] , %t );
//csize = size(data);
//assert_equal ( csize , [37 6] );

// TODO : test another separator, another comment caracter.


