// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if ( computed==%inf & expected==%inf ) then
    flag = 1
    return
  end
  if ( computed==-%inf & expected==-%inf ) then
    flag = 1
    return
  end
  if ( isnan(computed) & isnan(expected) ) then
    flag = 1
    return
  end
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if ( isnan(computed) & isnan(expected) ) then
    flag = 1
    return
  end
  if ( and(computed==expected) ) then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//---
x = 1;
[ f , e ] = flps_frexp ( x );
y = f * 2^e;
assert_equal ( x , y );
assert_equal ( f , 0.5 );
assert_equal ( e , 1 );
//---
b = 2;
x = 1;
[ f , e ] = flps_frexp ( x , b );
y = f * b^e;
assert_equal ( x , y );
assert_equal ( f , 0.5 );
assert_equal ( e , 1 );
//---
b = 3;
x = 1;
[ f , e ] = flps_frexp ( x , b , "v1" );
y = f * b^e;
assert_equal ( x , y );
assert_close ( f , 1/3 , %eps );
assert_equal ( e , 1 );
//---
b = 2;
x = 1;
[ f , e ] = flps_frexp ( x , b , "v2" );
y = f * b^e;
assert_equal ( x , y );
assert_equal ( f , 1 );
assert_equal ( e , 0 );
//---
// Matrices input
//
x = [1 2 3 4];
b = [3 3 3 3];
[ f , e ] = flps_frexp ( x , b , "v1" );
y = f .* b.^e;
assert_equal ( x , y );
assert_close ( f , [1/3    2/3    1/3    4/9] , %eps );
assert_equal ( e , [1.    1.    2.    2] );
//---
b = [3 3 3 3];
x = [1 2 3 4];
[ f , e ] = flps_frexp ( x , b , "v2" );
y = f .* b.^e;
assert_equal ( x , y );
assert_close ( f , [1.    2.    1.    4/3] , %eps );
assert_equal ( e , [0.    0.    1.    1] );
//
// With x scalar and b matrix
b = [2 3;5 7];
x = 2;
[ f , e ] = flps_frexp ( x , b );
y = f .* b.^e;
assert_equal ( x , y );
assert_close ( f , [1/2 2/3; 2/5 2/7] , %eps );
assert_equal ( e , [2 1; 1 1] );
//
// With b scalar and x matrix
x = [2 3;5 7];
b = 2;
[ f , e ] = flps_frexp ( x , b );
y = f .* b.^e;
assert_equal ( x , y );
assert_close ( f , [1/2 3/4; 5/8 7/8] , %eps );
assert_equal ( e , [2 2;3 3] );
//
// Some doubles f.p. constants
largestNormalM = 2^53-1; // 9007199254740991
smallestNormalM = 2^52;  // 4503599627370496
smallestnormal = smallestNormalM * 2^(-1022-53+1);   // 2.22507385850720138D-308
largestnormal = largestNormalM * 2^(1023-53+1);      // 1.79769313486231570D+308
smallestdenormal = 1 * 2^(-1022-53+1);               // 4.94065645841246544D-324
largestdenormal = 4503599627370495 * 2^(-1022-53+1); // 2.22507385850720088D-308
exp10largestM = largestNormalM * 2^(10-53+1);        // 2047.999999999999772626
exp10smallestM = smallestNormalM  * 2^(10-53+1);     // 1024

//---
//
// [x,b,f,e] for "v1"
//
expectedmatrix = [
 1   3  1/3  1
-1   3 -1/3  1
 2   3  2/3  1
-2   3 -2/3  1
 1/3 2  2/3 -1
-1/3 2 -2/3 -1
 8   2  1/2  4
-8   2 -1/2  4
 1/32   2   1/2 -4
-1/32   2  -1/2 -4
+0    2 +0   0
-0    2 -0   0
 %inf 2 +%inf 0
-%inf 2 -%inf 0
 %nan 2  %nan 0
-%nan 2  %nan 0
exp10largestM    2 0.9999999999999998889777  11
exp10smallestM   2 1/2                       11
smallestnormal   2 1/2                      -1021
largestnormal    2 0.9999999999999998889777  1024
smallestdenormal 2 1/2                      -1073
largestdenormal  2 0.9999999999999997779554 -1022
];
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = expectedmatrix(k,1);
  b = expectedmatrix(k,2);
  f = expectedmatrix(k,3);
  e = expectedmatrix(k,4);
  //
  // Check "v1"
  [ cf , ce ] = flps_frexp ( x , b , "v1" );
  if ( ce < 1024 ) then
    y = cf * b^ce;
  else
    y = cf * b * b^(ce-1);
  end
  assert_equal ( x , y );
  assert_equal ( cf , f );
  assert_equal ( ce , e );
  if ( abs(x)<>0 & ~isnan(x) & abs(x)<>%inf ) then
    assert_equal ( abs(f)>=1/b , %t );
    assert_equal ( abs(f)<1 , %t );
  end
end
//
// Test matrix input
//
x = expectedmatrix(:,1);
b = expectedmatrix(:,2);
f = expectedmatrix(:,3);
e = expectedmatrix(:,4);
[ cf , ce ] = flps_frexp ( x , b , "v1" );
assert_equal ( ce , e );
ck = find(isnan(cf));
k = find(isnan(f));
assert_equal ( ck , k );
ck = find(cf==%inf);
k = find(f==%inf);
assert_equal ( ck , k );
ck = find(cf==-%inf);
k = find(f==-%inf);
assert_equal ( ck , k );
ck = find(~isnan(cf) & abs(cf)<>%inf);
k = find(~isnan(f) & abs(f)<>%inf);
assert_equal ( cf(ck) , f(k) );

//---
//
// [x,b,f,e] for "v2"
//
expectedmatrix = [
 1   3  1  0
-1   3 -1  0
 2   3  2  0
-2   3 -2  0
 1/3 2  4/3  -2
-1/3 2 -4/3  -2
 8   2  1    3
-8   2 -1    3
 1/32   2   1 -5
-1/32   2  -1 -5
+0    2 +0   0
-0    2 -0   0
 %inf 2 +%inf 0
-%inf 2 -%inf 0
 %nan 2  %nan 0
-%nan 2  %nan 0
exp10largestM    2 1.9999999999999997779554  10
exp10smallestM   2 1                         10
smallestnormal   2 1                      -1022
largestnormal    2 1.9999999999999997779554  1023
smallestdenormal 2 1                        -1074
largestdenormal  2 1.9999999999999995559108 -1023
];
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = expectedmatrix(k,1);
  b = expectedmatrix(k,2);
  f = expectedmatrix(k,3);
  e = expectedmatrix(k,4);
  [ cf , ce ] = flps_frexp ( x , b , "v2" );
  if ( ce < 1024 ) then
    y = cf * b^ce;
  else
    y = cf * b * b^(ce-1);
  end
  assert_equal ( x , y );
  assert_equal ( cf , f );
  assert_equal ( ce , e );
  if ( abs(x)<>0 & ~isnan(x) & abs(x)<>%inf ) then
    assert_equal ( abs(f)>=1 , %t );
    assert_equal ( abs(f)<b , %t );
  end
end
//
// Test matrix input
//
x = expectedmatrix(:,1);
b = expectedmatrix(:,2);
f = expectedmatrix(:,3);
e = expectedmatrix(:,4);
[ cf , ce ] = flps_frexp ( x , b , "v2" );
assert_equal ( ce , e );
ck = find(isnan(cf));
k = find(isnan(f));
assert_equal ( ck , k );
ck = find(cf==-%inf);
k = find(f==-%inf);
assert_equal ( ck , k );
ck = find(~isnan(cf) & abs(cf)<>%inf);
k = find(~isnan(f) & abs(f)<>%inf);
assert_equal ( cf(ck) , f(k) );

