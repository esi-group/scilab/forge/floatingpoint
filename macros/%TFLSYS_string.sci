// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// %TFLSYS_string --
//   Returns the string containing the floating point system.
//
function str = %TFLSYS_string ( flps )
  str = []
  k = 1
  str(k) = sprintf("Floating Point System:\n")
  k = k + 1
  str(k) = sprintf("======================")
  k = k + 1
  str(k) = sprintf("radix= %s\n",  _tostring ( flps.radix ))
  k = k + 1
  str(k) = sprintf("p=     %s\n",  _tostring ( flps.p ) )
  k = k + 1
  str(k) = sprintf("emin=  %s\n",  _tostring ( flps.emin ) )
  k = k + 1
  str(k) = sprintf("emax=  %s\n",  _tostring ( flps.emax ) )
  k = k + 1
  str(k) = sprintf("vmin=  %s\n",  _tostring ( flps.vmin ) )
  k = k + 1
  str(k) = sprintf("vmax=  %s\n",  _tostring ( flps.vmax ) )
  k = k + 1
  str(k) = sprintf("eps=   %s\n",  _tostring ( flps.eps ) )
  k = k + 1
  str(k) = sprintf("r=     %s\n", _tostring ( flps.r ))
  k = k + 1
  str(k) = sprintf("gu=    %s\n", _tostring ( flps.gu ))
  k = k + 1
  str(k) = sprintf("alpha= %s\n", _tostring ( flps.alpha ))
  k = k + 1
  str(k) = sprintf("ebits= %s\n", _tostring ( flps.ebits ))
  k = k + 1
endfunction

function s = _tostring ( x )
  if ( x==[] ) then
    s = "[]"
  else
    n = size ( x , "*" )
    if ( n == 1 ) then
      s = string(x)
    else
      s = "["+strcat(string(x)," ")+"]"
    end
  end
endfunction


