//
// This help file was automatically generated from flps_double2hex.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_double2hex.sci
//

expected = "400921FB54442D18";
hexstr = flps_double2hex (%pi)
halt()   // Press return to continue

// Get and extract the binary string
[hexstr,binstr] = flps_double2hex (%pi)
sign_str = part(binstr,1) // sign_str="0"
expo_str = part(binstr,2:12) // expo_str="10000000000"
M_str = part(binstr,13:64) // M_str="1001001000011111101101010100010001000010110100011000"
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_double2hex.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
