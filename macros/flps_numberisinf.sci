// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function is = flps_numberisinf ( flpn )
  // Returns true if the number is an infinity.
  //
  // Calling Sequence
  //   is = flps_numberisinf ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   is : a 1x1 matrix of boolean
  //
  // Description
  //   Returns true if the number is an infinite.
  //   A +/- inf has an exponent equal to its maximum value and a zero integral
  //   significant.
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" );
  // // Get a %inf
  // flpn = flps_numbernew ( "double" , flps , %inf );
  // is = flps_numberisinf ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , -%inf );
  // is = flps_numberisinf ( flpn ) // %t
  //
  // // Get others
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // is = flps_numberisinf ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , +0 );
  // is = flps_numberisinf ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -0 );
  // is = flps_numberisinf ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberisinf ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // is = flps_numberisinf ( flpn ) // %f
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  is = ( flpn.e > flpn.flps.emax & abs(flpn.M) == 0 )
endfunction

