// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->


//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction
//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

expected = "400921FB54442D18";
hexstr = flps_double2hex (%pi);
assert_equal ( hexstr , expected ) ;
//
// Grey-box: the function uses the format function
format("e",25);
expected = "400921FB54442D18";
hexstr = flps_double2hex (%pi);
assert_equal ( hexstr , expected );
t = format();
assert_equal ( t , [0 25] );

//
// Get and extract the binary string
[hexstr,binstr] = flps_double2hex (%pi);
expected = "400921FB54442D18";
assert_equal ( hexstr , expected ) ;
expected = "0100000000001001001000011111101101010100010001000010110100011000";
assert_equal ( binstr , expected ) ;
//
sign_str = part(binstr,1);
expected = "0";
assert_equal ( sign_str , expected ) ;
//
expo_str = part(binstr,2:12);
expected = "10000000000";
assert_equal ( expo_str , expected ) ;
//
M_str = part(binstr,13:64);
expected = "1001001000011111101101010100010001000010110100011000";
assert_equal ( M_str , expected ) ;

//
// Simple tests
format("v",10);
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  hex = stripblanks(expectedmatrix(k,6));
  mprintf("[IEEEdouble] Test #%d/%d,x=%s\n",k,ntests,string(x));
  hexstr = flps_double2hex(x);
  assert_equal ( hexstr , hex ) ;
end


