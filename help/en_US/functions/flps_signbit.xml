<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from flps_signbit.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="flps_signbit" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>flps_signbit</refname><refpurpose>Returns the sign bit of x</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   b = flps_signbit ( x )

   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>x :</term>
      <listitem><para> a matrix of doubles</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a matrix of booleans. b(i,j)=%f if the sign bit of x(i,j) is 0 (i.e. x is positive). b(i,j)=%t if the sign bit of x(i,j) is 1 (i.e. x is negative).</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
This functions allows to inquire about the sign bit of x.
The sign bit s is so that the sign of x is (-1)^s.
   </para>
   <para>
This allows to overcome the sign function, which returns the same
value for -0 and -0.
   </para>
   <para>
This function works well for +/- inf.
For +/-%nan, this function returns %f.
The IEEE standard does not specify the sign bit of a nan.
See in the section 6.3 "The sign bit" of the IEEE 754-2008 Standard.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
b = flps_signbit ( 1 ) // %f
b = flps_signbit ( -1 ) // %t
b = flps_signbit ( 0 ) // %f
b = flps_signbit ( -0 ) // %t


x =        [+0 -0 %inf -%inf %nan -%nan];
expected = [%f %t %f   %t    %f   %f]
b = flps_signbit ( x )

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, 2008 - 2010</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>"Handbook of floating-point arithmetic", Jean-Michel Muller, Nicolas Brisebarre, Florent de Dinechin, Claude-Pierre Jeannerod, Vincent Lefevre, Guillaume Melquiond, Nathalie Revol, Damien Stehle, Serge Torres, Birkhauser Boston, 2010</para>
   <para>"IEEE Standard for Floating-Point Arithmetic", IEEE Computer Society, IEEE Std 754-2008</para>
</refsection>
</refentry>
