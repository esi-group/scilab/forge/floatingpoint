// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function data = flps_datasetread ( varargin )
    // Reads the strings from a dataset.
    //
    // Calling Sequence
    //   data = flps_datasetread ( filename )
    //   data = flps_datasetread ( filename , comstr )
    //   data = flps_datasetread ( filename , comstr , sepstr )
    //   data = flps_datasetread ( filename , comstr , sepstr , verbose )
    //
    // Parameters
    //   filename : a 1x1 matrix of strings, the file name
    //   comstr : a 1x1 matrix of strings, the caracter used in the first column of the file to identify the comments. Default comstr = "#".
    //   sepstr : a 1x1 matrix of strings, the caracter to identify the columns of the dataset.  Default sepstr = ",".
    //   verbose : a 1x1 matrix of booleans, set to %t to display intermediate messages. Default verbose = %f.
    //   data : a matrix of strings
    //
    // Description
    //   Consider the file as a dataset containing comments and datas and
    //   returns the data from the dataset.
    //
    //   TODO : returns the comments as second output argument.
    //
    // Examples
    // path=flps_getpath();
    // dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv")
    // data = flps_datasetread ( dataset , "#" , "," , %f );
    // size(data)
    // // Get the first column
    // x = evstr(data(:,1))
    //
    // Authors
    // Copyright (C) 2010 - DIGITEO - Michael Baudin
    //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_datasetread" , rhs , 1:4 )
  apifun_checklhs ( "flps_datasetread" , lhs , 1 )
  //
  filename = varargin(1)
  comstr = argindefault ( rhs , varargin , 2 , "#" )
  sepstr = argindefault ( rhs , varargin , 3 , "," )
  verbose = argindefault ( rhs , varargin , 4 , %f )
  //
  apifun_checktype ( "flps_datasetread" , filename , "filename" , 1 , "string" )
  apifun_checktype ( "flps_datasetread" , comstr , "comstr" , 2 , "string" )
  apifun_checktype ( "flps_datasetread" , sepstr , "sepstr" , 3 , "string" )
  apifun_checktype ( "flps_datasetread" , verbose , "verbose" , 4 , "boolean" )
  //
  // Proceed...
    v = mgetl(filename)
    nrows = size(v,"r")
    if ( verbose ) then
        mprintf("Dataset : %s\n",filename)
        mprintf("Number of lines : %d\n",nrows)
    end
    //
    // Remove comment lines
    for i = 1 : size(v,'*')
        line = v(i);
        firstchar = part(line,1)
        if ( firstchar == comstr ) then
            v(i) = ""
        end
    end
    //
    // Remove empty lines
    v(v == '') = [];
    //
    ns = length(sepstr);
    data = [];
    ki = 1;
    for i = 1 : size(v,'*')
        line = v(i);
        K = [strindex(line, sepstr)];
        ki = 1;
        row = [];
        for k = K
            row = [row, part(line,ki:k-1)];
            ki = k + ns;
        end
        row = [row, part(line, ki:length(line))];
        if (i > 1) then
            if ( size(row,2) > size(data,2) ) then
                data($,size(row,2)) = '';
            elseif ( size(row,2) < size(data,2) ) then
                row(1, size(data,2)) = '';
            end
        end
        data = [data; row];
    end
    data = strsubst(data,"I","%i")
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

