// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
expectedmatrix = [
"%nan"    "%t"
"-%nan"   "%t"
"%inf"    "%f"
"-%inf"   "%f"
"+0"      "%f"
"-0"      "%f"
"+1"      "%f"
"-1"      "%f"
"1.7976931348623157e+308" "%f" // largest normal
"2.2250738585072014e-308" "%f" // smallest normal
"2.2250738585072008e-308" "%f" // largest denormal
"4.9406564584124654e-324" "%f" // smallest denormal
"-1.7976931348623157e308"  "%f" // largest negative normal
"-2.2250738585072014e-308" "%f" // smallest negative normal
"-2.2250738585072008e-308" "%f" // largest negative denormal
"-4.9406564584124654e-324" "%f" // smallest negative denormal
];

ntests = size(expectedmatrix,"r");
flps = flps_systemnew("IEEEdouble");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  flpn = flps_numbernew ( "double" , flps , x );
  expected = evstr(expectedmatrix(k,2));
  is = flps_numberisnan(flpn);
  assert_equal ( is , expected ) ;
end


