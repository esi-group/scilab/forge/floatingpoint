<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 * Copyright (C) 2010 - DIGITEO - Michael Baudin
-->
<refentry
  xmlns="http://docbook.org/ns/docbook"
  xmlns:xlink="http://www.w3.org/1999/xlink"
  xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:mml="http://www.w3.org/1998/Math/MathML"
  xmlns:db="http://docbook.org/ns/docbook"
  version="5.0-subset Scilab"
  xml:lang="fr"
  xml:id="floatingpoint_systemoverview">

  <refnamediv>
    <refname>Floatingpoint system/number overview</refname>

    <refpurpose>An overview of the Floatingpoint system/number components.</refpurpose>
  </refnamediv>

  <refsection>
    <title>Purpose</title>

    <para>
    The goal of these functions is to be able to manage virtual floating point systems.
    Once a system is defined, we can create floating point numbers represented in this system.
    </para>

    <para>
     The <literal>flps_systemnew</literal> function creates floating point system while the <literal>flps_numbernew</literal>
     function creates floating point numbers.
     The <literal>flps_systemgui</literal> function allows to see the numbers which can be
     represented on a floating point system.
     It is based on the <literal>flps_systemall</literal> function, which computes and returns
     all the floating point numbers which can be represented on a floating point system.
    </para>

  </refsection>

  <refsection>
    <title>Creating a floating point system</title>

    <para>
    The <literal>flps_systemnew</literal> function creates an empty floating point system.
    The minimum parameters defining a floating point system are: the radix r, the precision p, the
    exponent range emin and emax.
    </para>

    <para>
    Additionnally, the rounding mode can take one of the 4 values being defined in the
    IEEE 754 standard.
    The default rounding mode is round-to-nearest.
    Additionnally, the f. p. system may support denormalized numbers.
    The other properties of the f. p. system are computed from these parameters, including the
    machine epsilon eps, the maximum value before overflow vmax, the smallest positive subnormal
    number alpha and the minimum normal value before underflow.
    </para>

    <para>
    The minimum and maximum exponents are
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    e_{min} = -\beta^{ebits-1} + 2 \\
    e_{max} = \beta^{ebits-1}-1
    \end{eqnarray}
    </latex>
    </para>

    <para>
    where ebits is the number of bits in the exponent.
    </para>

    <para>
    The smallest positive normal floating point number (called vmin in flps_numbernew) is
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    \mu = \beta^{e_{min}}.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    The largest positive normal floating point number (called vmax in flps_numbernew) is
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    \Omega = (\beta - \beta^{1-p}) \beta^{e_{max}}.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    The smallest positive subnormal floating point number is
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    \alpha = \beta^{e_{min}-p+1}.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    The spacing between the floating point number x=1 and the next larger
    floating point number is the machine epsilon (called eps in flps_numbernew), which
    satisfies
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    \epsilon = \beta^{1-p}.
    \end{eqnarray}
    </latex>
    </para>

  </refsection>

  <refsection>
    <title>Creating a floating point number</title>

    <para>
    Once that a f. p. system has been defined, we can define f. p. numbers into
    this system.
    The <literal>flps_numbernew</literal> function creates a floating point number either
    empty or based on a particular representation.
    </para>

    <para>
    The calling sequence <literal>flpn = flps_numbernew ( "integral" , flps , M , e )</literal>
    creates a f.p. number from a f.p. system and its integral form.
    This creates the number x such that
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    x = M \cdot \beta^{e-p+1},
    \end{eqnarray}
    </latex>
    </para>

    <para>
    where M is the integral significant, e is the exponent, beta is the radix and p is the precision.
    </para>

    <para>
    The integral significant M satisfies the inequality
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    |M| &lt; \beta^p.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    while the exponent e satisfies
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    e_{min} \leq e \leq e_{max}.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    The calling sequence <literal>flpn = flps_numbernew ( "signm" , flps , s , m , e)</literal>
    creates a f.p. number from a f.p. system and its sign-significant form.
    This creates the number x such that
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    x = (-1)^s \cdot m \cdot \beta^e
    \end{eqnarray}
    </latex>
    </para>

    <para>
    where s is the sign, m is the normal significant and e is the exponent.
    </para>

    <para>
    The normal significand m satisfies the inequalities
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    0 \leq m &lt; \beta
    \end{eqnarray}
    </latex>
    </para>

    <para>
    and the exponent e satisfies the inequalities
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    e_{min} \leq e \leq e_{max}.
    \end{eqnarray}
    </latex>
    </para>

    <para>
    For a normalized floating point number, the integral significant satisfies:
    </para>

    <para>
    <latex>
    \begin{eqnarray}
    \beta^{p-1} \leq |M| &lt; \beta^p.
    \end{eqnarray}
    </latex>
    </para>

  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>2008-2009 - INRIA - Michael Baudin</member>
      <member>2010 - DIGITEO - Michael Baudin</member>
    </simplelist>
  </refsection>

</refentry>
