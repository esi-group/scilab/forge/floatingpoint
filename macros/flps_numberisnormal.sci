// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function is = flps_numberisnormal ( flpn )
  // Returns true if the number is bnormal.
  //
  // Calling Sequence
  //   is = flps_numberisnormal ( flpn )
  //
  // Parameters
  //   flpn : a floating point number
  //   is : a 1x1 matrix of boolean
  //
  // Description
  //   Returns true if the number is a normal number, that is
  //   not a nan, not an infinity, not a subnormal, not a zero.
  //   The biased exponent E of a normal number is stricly in its range.
  //
  // Examples
  // flps = flps_systemnew ( "IEEEdouble" )
  // // Get normal
  // flpn = flps_numbernew ( "double" , flps , 1 );
  // is = flps_numberisnormal ( flpn ) // %t
  // flpn = flps_numbernew ( "double" , flps , -1 );
  // is = flps_numberisnormal ( flpn ) // %t
  //
  // // Get others
  // flpn = flps_numbernew ( "double" , flps , %nan );
  // is = flps_numberisnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , +0 );
  // is = flps_numberisnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , -0 );
  // is = flps_numberisnormal ( flpn ) // %f
  // flpn = flps_numbernew ( "double" , flps , 5.e-320 )
  // is = flps_numberisnormal ( flpn ) // %f
  //
  // Authors
  // Copyright (C) 2010 - DIGITEO - Michael Baudin
  //

  is = ( flpn.e <= flpn.flps.emax & flpn.m >= 1 )
endfunction

