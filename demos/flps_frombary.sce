//
// This help file was automatically generated from flps_frombary.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_frombary.sci
//

x = flps_frombary ( [1 0]' )
expected = 1
//
x = flps_frombary ( [1 0]' , 2 )
expected = 1
//
x = flps_frombary ( [0 1 0 1]' , 3 )
expected = 1/3+1/3^3
//
x = flps_frombary ( [1 1 0 1 0 1]' )
expected = 1 + 1/2 + 1/2^3 + 1/2^5
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_frombary.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
