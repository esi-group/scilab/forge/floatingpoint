//
// This help file was automatically generated from flps_numbereval.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_numbereval.sci
//

// Evaluate one floating point number
x = 4;
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e )
flpn = flps_numbernew ( "double" , flps , x )
// This should be f = 4
f = flps_numbereval ( flpn )
halt()   // Press return to continue

// Evaluate all the floating point numbers in a system
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
listflpn = flps_systemall ( flps );
// This should be -14, -12, -10, -8, ..., 12, 14
f = flps_numbereval ( listflpn )
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_numbereval.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
