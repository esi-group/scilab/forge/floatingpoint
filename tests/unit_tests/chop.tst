// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
  if computed==expected then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

// Uses default t and b

c = flps_chop ( 1/3 );
assert_close ( c , 0.33333334326744080 , %eps );

// Check that matrices which contain zeros are correctly chopped

c = flps_chop ( [1/3 0 0 1 2 1/5] );
assert_close ( c , [ 0.33333334326744080    0.    0.    1.    2.    0.20000000298023224] , %eps );

// Check that the precision is taken into account.
// Simulate a Cray - 1

c = flps_chop ( [1/3 0 0 1 2 1/5] , 48 );
assert_close ( c , [0.33333333333333393    0.    0.    1.    2.    0.20000000000000018] , %eps );

// Check that the basis is taken into account.
// Simulate a theoretical system

c = flps_chop ( 0.123456789 , 3 , 10 );
assert_close ( c , 0.123 , %eps );

