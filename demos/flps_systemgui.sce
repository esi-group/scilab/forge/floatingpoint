//
// This help file was automatically generated from flps_systemgui.sci using help_from_sci().
// PLEASE DO NOT EDIT
//
mode(1)
//
// Demo of flps_systemgui.sci
//

// A toy system, including denormals and negative
radix = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , radix , p , e );
h = flps_systemgui ( flps );
// Wait : see the picture before closing it...
close(h);
halt()   // Press return to continue

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "flps_systemgui.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
