// Copyright (C) 2008 - 2010 - Michael Baudin
// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function c = flps_chop ( varargin )
  //   Round matrix elements to t significant binary places.
  //
  // Calling Sequence
  // c = flps_chop ( x )
  // c = flps_chop ( x , t )
  // c = flps_chop ( x , t , b )
  //
  // Parameters
  //   x : a matrix of doubles, the matrix to chop
  //   t : a 1x1 matrix of floating point integers, must be positive, the number of significant bits to chop. Default is t = 24, corresponding to IEEE single precision.
  //   b : a 1x1 matrix of floating point integers, must be greater than 1, the radix
  //
  // Description
  //   This function allows to simulate a system with a
  //   lower precision than doubles.
  //
  // Any optional input argument equal to the empty matrix is replaced by its default value.
  //
  // Examples
  //  format("v",25)
  //  // See 1/3 before choping
  //  1/3
  //  c = flps_chop ( 1/3 )
  //  c = flps_chop ( [1/3 0 0 1 2 1/5] )
  //  c = flps_chop ( [1/3 0 0 1 2 1/5] , 48 )
  //  c = flps_chop ( 0.123456789 , 3 , 10 )
  //
  // Authors
  // Nicolas Higham, Matlab Version
  // Michael Baudin, 2010, Scilab port
  //
  // Bibliography
  //   "The Test Matrix Toolbox for Matlab (Version 3.0)", Nicolas Higham, Numerical Analysis Report No. 276, September 1995, Manchester Centre for Computational Mathematics, The University of Manchester
  //   "Higham's Test Matrices ", John Burkardt, 2005, http://people.sc.fsu.edu/~burkardt/m_src/test_matrix/test_matrix.html
  //

  [lhs, rhs] = argn()
  apifun_checkrhs ( "flps_chop" , rhs , 1:3 )
  apifun_checklhs ( "flps_chop" , lhs , 1 )
  //
  x = varargin(1)
  t = argindefault ( rhs , varargin , 2 , 24 )
  b = argindefault ( rhs , varargin , 3 , 2 )
  //
  apifun_checktype ( "flps_chop" , x , "x" , 1 , "constant" )
  apifun_checktype ( "flps_chop" , t , "t" , 2 , "constant" )
  apifun_checktype ( "flps_chop" , b , "b" , 3 , "constant" )
  //
  apifun_checkscalar ( "flps_chop" , t , "t" , 2 )
  apifun_checkscalar ( "flps_chop" , b , "b" , 3 )
  //
  apifun_checkgreq ( "flps_chop" , t , "t" , 2 , 0 )
  apifun_checkgreq ( "flps_chop" , b , "b" , 3 , 2 )
  //
  //  Use the representation:
  //  x(i,j) = 2^e(i,j) * .d(1)d(2)...d(s) * sign(x(i,j))
  nz = find(x<>0)
  y(nz) = abs(x(nz));
  e(nz) = floor(log(y(nz))/log(b) + 1);
  p(nz) = x(nz).*b^(t-e(nz))
  c(nz) = round(p(nz)) .* b .^ (e(nz)-t);
  zz = find(x==0)
  c(zz) = 0
endfunction
function argin = argindefault ( rhs , vararglist , ivar , default )
  // Returns the value of the input argument #ivar.
  // If this argument was not provided, or was equal to the
  // empty matrix, returns the default value.
  if ( rhs < ivar ) then
    argin = default
  else
    if ( vararglist(ivar) <> [] ) then
      argin = vararglist(ivar)
    else
      argin = default
    end
  end
endfunction

