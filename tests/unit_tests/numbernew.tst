// Copyright (C) 2008 - 2010 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

//
// assert_close --
//   Returns 1 if the two real matrices computed and expected are close,
//   i.e. if the relative distance between computed and expected is lesser than epsilon.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_close ( computed, expected, epsilon )
  if expected==0.0 then
    shift = norm(computed-expected);
  else
    shift = norm(computed-expected)/norm(expected);
  end
  if shift < epsilon then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

//
// assert_equal --
//   Returns 1 if the two real matrices computed and expected are equal.
// Arguments
//   computed, expected : the two matrices to compare
//   epsilon : a small number
//
function flag = assert_equal ( computed , expected )
    if ( and(isnan(computed)) & and(isnan(expected)) ) then
        flag = 1
    else
        if (computed==expected) then
            flag = 1;
        else
            flag = 0;
        end
    end
    if flag <> 1 then pause,end
endfunction

function flag = assert_true ( computed )
  if computed then
    flag = 1;
  else
    flag = 0;
  end
  if flag <> 1 then pause,end
endfunction

function numbercheckfields ( flpn , x , M , e , m , s , hex )
  //
  // Check all fields of the current floating point number
  assert_equal ( flpn.M , M );
  assert_equal ( flps_signbit(flpn.M) , flps_signbit(M) );
  assert_equal ( flpn.e , e );
  assert_equal ( flpn.m , m );
  assert_equal ( flps_signbit(flpn.m) , %f );
  assert_equal ( flpn.s , s );
  //
  chex = flps_number2hex(flpn);
  assert_equal ( chex , hex ) ;
  //
  computed = flps_numbereval(flpn)
  assert_equal ( computed , x )

endfunction

////////////////////////////////////////////////////////////////////
//
// Also tests the consistency with:
// * flps_systemnew
// * flps_numbernew
// * flps_number2hex
// * flps_numbereval
//
////////////////////////////////////////////////////////////////////

// Check the default constructor and the printer system
flpn = flps_numbernew ()
flpn.s = 0

// A toy system, including denormals and negative
r = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , r , p , e );
flpn = flps_numbernew ( "integral" , flps , 4 , 2 );
assert_equal ( flpn.M , 4 );
assert_equal ( flpn.e , 2 );
assert_equal ( flpn.s , 0 );
assert_equal ( flpn.m , 1 );
//
// Generate the string, but do not display it.
str = string(flpn);

// A toy system, including denormals and negative
r = 2;
p = 3;
e = 3;
flps = flps_systemnew ( "format" , r , p , e );
flpn = flps_numbernew ( "signm" , flps , 0 , 1 , 2 );
assert_equal ( flpn.M , 4 );
assert_equal ( flpn.e , 2 );
assert_equal ( flpn.s , 0 );
assert_equal ( flpn.m , 1 );
//
// Generate the string, but do not display it.
str = string(flpn);

/////////////////////////////////////////////////////////////
// Check rounding modes : default mode is round to nearest
flps = flps_systemnew ( "format" , 2 , 3 , 3 );

flpn = flps_numbernew ( "double" , flps , 0.54 );
assert_equal ( flpn.M , 4 );
//
flpn = flps_numbernew ( "double" , flps , 0.60 );
assert_equal ( flpn.M , 5 );
//
flpn = flps_numbernew ( "double" , flps , -0.54 );
assert_equal ( flpn.M , -4 );
//
flpn = flps_numbernew ( "double" , flps , -0.60 );
assert_equal ( flpn.M , -5 );

// A case of tie: check that round to even is applied
x= 4.5 * 2^(-1-3+1);
flpn = flps_numbernew ( "double" , flps , x );
assert_equal ( flpn.M , 4 );

// A case of tie: check that round to even is applied
x= 4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x );
assert_equal ( flpn.M , 4 );

// A case of tie: check that round to even is applied
x= -4.5 * 2^(0-3+1);
flpn = flps_numbernew ( "double" , flps , x );
assert_equal ( flpn.M , -4 );

// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.49 );
assert_equal ( flpn.M , 4 );
assert_equal ( flpn.e , -1 );

/////////////////////////////////////////////////////////////

// Check rounding modes : set rounding mode to round up
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 2;

flpn = flps_numbernew ( "double" , flps , 0.54 );
assert_equal ( flpn.M , 5 );
//
flpn = flps_numbernew ( "double" , flps , 0.60 );
assert_equal ( flpn.M , 5 );
//
flpn = flps_numbernew ( "double" , flps , -0.54 );
assert_equal ( flpn.M , -4 );
//
flpn = flps_numbernew ( "double" , flps , -0.60 );
assert_equal ( flpn.M , -4 );

// A case where the number is rounded up, which makes the exponent increase
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , 0.99 );
assert_equal ( flpn.M , 4 );
assert_equal ( flpn.e , 0 );

/////////////////////////////////////////////////////////////

// Check rounding modes : set rounding mode to round down
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 3;

flpn = flps_numbernew ( "double" , flps , 0.54 );
assert_equal ( flpn.M , 4 );
//
flpn = flps_numbernew ( "double" , flps , 0.60 );
assert_equal ( flpn.M , 4 );
//
flpn = flps_numbernew ( "double" , flps , -0.54 );
assert_equal ( flpn.M , -5 );
//
flpn = flps_numbernew ( "double" , flps , -0.60 );
assert_equal ( flpn.M , -5 );

// A case where the number is rounded up, which makes the exponent decrease
// to keep the number normalized
flpn = flps_numbernew ( "double" , flps , -0.49 );
assert_equal ( flpn.M , -4 );
assert_equal ( flpn.e , -1 );

/////////////////////////////////////////////////////////////

// Check rounding modes : set rounding mode to round to zero
flps = flps_systemnew ( "format" , 2 , 3 , 3 );
flps.r = 4;

flpn = flps_numbernew ( "double" , flps , 0.54 );
assert_equal ( flpn.M , 4 );
//
flpn = flps_numbernew ( "double" , flps , 0.60 );
assert_equal ( flpn.M , 4 );
//
flpn = flps_numbernew ( "double" , flps , -0.54 );
assert_equal ( flpn.M , -4 );
/////////////////////////////////////////////////////////////////////////
//
// Tests for singles
//
flps = flps_systemnew("IEEEsingle");
//
// [x M e m s]
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEsingle.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  M = evstr(expectedmatrix(k,2));
  e = evstr(expectedmatrix(k,3));
  m = evstr(expectedmatrix(k,4));
  s = evstr(expectedmatrix(k,5));
  hex = stripblanks(expectedmatrix(k,6));
  //
  mprintf("%-30s Test #%d/%d,x=%s\n","[IEEEsingle-double]",k,ntests,string(x));
  flpn = flps_numbernew ( "double" , flps , x );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,M=%s,e=%s\n","[IEEEsingle-integral]",k,ntests,string(M),string(e));
  flpn = flps_numbernew ( "integral" , flps , M , e );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,s=%s,m=%s,e=%s\n","[IEEEsingle-signm]",k,ntests,string(s),string(m),string(e));
  flpn = flps_numbernew ( "signm" , flps , s , m , e );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,hex=%s\n","[IEEEsingle-hex]",k,ntests,hex);
  flpn = flps_numbernew ( "hex" , flps , hex );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  // Generate the string, but do not display it.
  str = string(flpn);
end
/////////////////////////////////////////////////////////////
//
// Check formatting of doubles
//
// Some doubles f.p. constants
flps = flps_systemnew("IEEEdouble");
//
path=flps_getpath();
dataset = fullfile(path,"tests","unit_tests","dataset_IEEEdouble.csv");
expectedmatrix = flps_datasetread ( dataset , "#" , "," , %f );
ntests = size(expectedmatrix,"r");
for k = 1 : ntests
  x = evstr(expectedmatrix(k,1));
  M = evstr(expectedmatrix(k,2));
  e = evstr(expectedmatrix(k,3));
  m = evstr(expectedmatrix(k,4));
  s = evstr(expectedmatrix(k,5));
  hex = stripblanks(expectedmatrix(k,6));
  //
  mprintf("%-30s Test #%d/%d,x=%s\n","[IEEEdouble-double]",k,ntests,string(x));
  flpn = flps_numbernew ( "double" , flps , x );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,M=%s,e=%s\n","[IEEEdouble-integral]",k,ntests,string(M),string(e));
  flpn = flps_numbernew ( "integral" , flps , M , e );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,s=%s,m=%s,e=%s\n","[IEEEdouble-signm]",k,ntests,string(s),string(m),string(e));
  flpn = flps_numbernew ( "signm" , flps , s , m , e );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  mprintf("%-30s Test #%d/%d,hex=%s\n","[IEEEsingle-hex]",k,ntests,hex);
  flpn = flps_numbernew ( "hex" , flps , hex );
  numbercheckfields ( flpn , x , M , e , m , s , hex );
  //
  // Generate the string, but do not display it.
  str = string(flpn);
end


//
// Create extreme numbers
//
// Create +Infinity
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , +0 , flps.emax + 1 );
is = flps_numberisinf(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %f );
//
// Create -Infinity
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , -0 , flps.emax + 1 );
is = flps_numberisinf(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %t );
//
// Create Nan
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , +1 , flps.emax + 1 );
is = flps_numberisnan(flpn);
assert_true ( is );
//
// Create +0
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , +0 , flps.emin );
is = flps_numberiszero(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %f );
//
// Create -0
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , -0 , flps.emin );
is = flps_numberiszero(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %t );
//
// Create Largest positive subnormal
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , flps.radix^(flps.p-1)-1 , flps.emin );
is = flps_numberissubnormal(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %f );
//
// Create Largest negative subnormal
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , -(flps.radix^(flps.p-1)-1) , flps.emin );
is = flps_numberissubnormal(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %t );
//
// Create Smallest positive subnormal
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , +1 , flps.emin );
is = flps_numberissubnormal(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %f );
//
// Create Smallest negative subnormal
flps = flps_systemnew("IEEEdouble");
flpn = flps_numbernew ( "integral" , flps , -1 , flps.emin );
is = flps_numberissubnormal(flpn);
assert_true ( is );
assert_equal ( flps_signbit(flpn.M) , %t );
//
// Test error cases
//
// We are not able to create a number with subnormal integral significant M and
// an exponent not equal to the minimum.
format("v",25);
flps = flps_systemnew("IEEEdouble");
instr = "flps_numbernew ( ""integral"" , flps , 4503599627370495 , 0 )";
ierr = execstr(instr,"errcatch");
laerr = lasterror();
assert_equal ( ierr , 10000 );
assert_equal ( laerr , "flps_numbernew: Exponent e=0 is for a normal number, but abs(M)=4503599627370495 in not in the range [4503599627370496,9007199254740991].");
//
// We are not able to create a number with subnormal normal significant m and
// an exponent not equal to the minimum.
format("v",25);
flps = flps_systemnew("IEEEdouble");
instr = "flps_numbernew ( ""signm"" , flps , 0 , 0 , 0 )";
ierr = execstr(instr,"errcatch");
laerr = lasterror();
assert_equal ( ierr , 10000 );
assert_equal ( laerr , "flps_numbernew: Exponent e=0 is for a normal number, but m=0 in not in the range [1,2].");

